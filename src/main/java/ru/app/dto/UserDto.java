package ru.app.dto;

import lombok.Builder;
import lombok.Data;
import ru.app.models.Task;

import java.util.List;

@Data
@Builder
public class UserDto {
    private Long id;
    private String name;
    private String login;
    private List<Task> tasks;

}
