package ru.app.controllers.handlers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.app.exceptions.*;

@ControllerAdvice
public class ExceptionHandlingController {

    @ExceptionHandler(CreatingATaskInThePastTimeException.class)
    public ResponseEntity<String> handleException1() {
          return ResponseEntity.status(403).body("You can't create a task in the past time!");
    }

    @ExceptionHandler(IncorrectDateFormatException.class)
    public ResponseEntity<String> handleException2() {
        return ResponseEntity.status(400).body("The date format is specified incorrectly <<1970.01.01 12:00>>!");
    }

    @ExceptionHandler(TheTaskTimeIsSpecifiedIncorrectlyException.class)
    public ResponseEntity<String> handleException3() {
        return ResponseEntity.status(403).body("The task cannot end before it starts!");
    }

    @ExceptionHandler(TimeConflictInTheConstructionOfTasksException.class)
    public ResponseEntity<String> handleException4() {
        return ResponseEntity.status(403).body("At this time, there is already a scheduled task!");
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<String> handleException5(UserNotFoundException e) {
        return ResponseEntity.status(404).body(e.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleException6(IllegalArgumentException e) {
        return ResponseEntity.status(500).body(e.getMessage());
    }

    @ExceptionHandler(LetTheListOfUsersException.class)
    public ResponseEntity<String> handleException7() {
        return ResponseEntity.status(400).body("Specify at least one user ID!");
    }

    @ExceptionHandler(ThisLoginAlreadyExistsException.class)
    public ResponseEntity<String> handleException8(ThisLoginAlreadyExistsException e) {
        return ResponseEntity.status(403).body(e.getMessage());
    }

}
