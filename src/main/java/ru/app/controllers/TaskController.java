package ru.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.app.mappers.TaskMapper;
import ru.app.dto.TaskDto;
import ru.app.forms.TaskForm;
import ru.app.services.TaskService;
import ru.app.utils.TimeIntervals;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/")
public class TaskController {

    private TaskService taskService;

    private TaskMapper taskMapper;

    @Autowired
    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    @Autowired
    public void setTaskMapper(TaskMapper taskMapper) {
        this.taskMapper = taskMapper;
    }

    @RequestMapping(value = "/tasks", method = RequestMethod.POST)
    public ResponseEntity<TaskDto> addTaskUser(@RequestBody TaskForm taskForm){
       return ResponseEntity.ok(taskMapper.parser(taskService.createTaskUser(taskForm.getUsersId() , taskMapper.parser(taskForm))));
    }


    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public ResponseEntity<List<TimeIntervals>> getUsersFreeTime(@RequestBody Set<Long> userId){
        return ResponseEntity.ok(taskService.getFreeTime(userId));
    }

}
