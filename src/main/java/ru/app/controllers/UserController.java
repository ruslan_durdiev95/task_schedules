package ru.app.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.app.mappers.UserMapper;
import ru.app.services.UserService;
import ru.app.dto.UserDto;
import ru.app.forms.UserForm;

@RestController
@RequestMapping("/")
public class UserController {

    private UserService userService;

    private UserMapper userMapper;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<UserDto> createUser(@RequestBody UserForm userForm){
        return ResponseEntity.ok(userMapper.parser(userService.addUser(userMapper.parser(userForm))));
    }

}
