package ru.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.app.models.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query("SELECT t FROM Task t WHERE t.endDate >= CURRENT_DATE and t.user.id = ?1")
    List<Task> getTaskByUserId(Long userId);

    @Transactional
    void deleteByTitle(String title);

}
