package ru.app.exceptions;

public class TheTaskTimeIsSpecifiedIncorrectlyException extends RuntimeException{

    private String message;

    public TheTaskTimeIsSpecifiedIncorrectlyException(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
