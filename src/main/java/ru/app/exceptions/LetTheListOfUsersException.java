package ru.app.exceptions;

public class LetTheListOfUsersException extends RuntimeException{
    private String message;

    public LetTheListOfUsersException(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
