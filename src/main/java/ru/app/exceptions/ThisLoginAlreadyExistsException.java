package ru.app.exceptions;

public class ThisLoginAlreadyExistsException extends RuntimeException{
    private String message;

    public ThisLoginAlreadyExistsException(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
