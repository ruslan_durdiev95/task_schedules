package ru.app.exceptions;

public class CreatingATaskInThePastTimeException extends RuntimeException{

    private String message;

    public CreatingATaskInThePastTimeException(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
