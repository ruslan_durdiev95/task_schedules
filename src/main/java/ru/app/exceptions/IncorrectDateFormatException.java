package ru.app.exceptions;

public class IncorrectDateFormatException extends RuntimeException{

    private String message;

    public IncorrectDateFormatException(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
