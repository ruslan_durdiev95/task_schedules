package ru.app.exceptions;

public class TimeConflictInTheConstructionOfTasksException extends RuntimeException {
    private String message;

    public TimeConflictInTheConstructionOfTasksException(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
