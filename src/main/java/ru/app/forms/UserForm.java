package ru.app.forms;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserForm {
    private String name;
    private String login;
}
