package ru.app.forms;

import lombok.Builder;
import lombok.Data;

import java.util.Set;


@Data
@Builder
public class TaskForm {
    private String title;
    //формат 2021.08.10 12:00
    private String startDate;
    //формат 2021.08.10 12:00
    private String endDate;
    private Set<Long> usersId;
}
