package ru.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.app.exceptions.CreatingATaskInThePastTimeException;
import ru.app.exceptions.LetTheListOfUsersException;
import ru.app.exceptions.TheTaskTimeIsSpecifiedIncorrectlyException;
import ru.app.exceptions.TimeConflictInTheConstructionOfTasksException;
import ru.app.models.Task;
import ru.app.models.User;
import ru.app.repositories.TaskRepository;
import ru.app.utils.TimeIntervals;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TaskServiceImpl implements TaskService {

    private TaskRepository taskRepository;
    private UserService userService;

    @Autowired
    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Task createTaskUser(Set<Long> userId, Task task) {
        long startDate = task.getStartDate().getTime();
        long endDate = task.getEndDate().getTime();

        for (Long id : userId) {
            Task taskClone;
            try {
                taskClone = task.clone();
            } catch (CloneNotSupportedException e) {
                throw new IllegalArgumentException(e);
            }

            User user = userService.getUser(id);

            if (startDate >= endDate)
                throw new TheTaskTimeIsSpecifiedIncorrectlyException("the task cannot end before it starts");

            Date now = new Date();
            if (startDate < now.getTime())
                throw new CreatingATaskInThePastTimeException("you can't create a task in the past time");

            List<Task> tasks = taskRepository.getTaskByUserId(id);
            if (tasks != null) {
                for (Task tas : tasks) {
                    long startDateFromList = tas.getStartDate().getTime();
                    long endDateFromList = tas.getEndDate().getTime();
                    if (startDateFromList < endDate && endDateFromList > startDate)
                        throw new TimeConflictInTheConstructionOfTasksException("at this time, there is already a scheduled task");
                }
            }
            taskClone.setUser(user);
            taskRepository.saveAndFlush(taskClone);
        }
        return task;
    }

    @Override
    public List<TimeIntervals> getFreeTime(Set<Long> userId) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");

        long startingPosition ;

        List<TimeIntervals> timeIntervals = new ArrayList<>();

        List<Task> allTasks = new ArrayList<>();

        List<Task> currentTasks = new ArrayList<>();

        if (userId.isEmpty())
            throw new LetTheListOfUsersException("Empty list of users");

        for (Long id : userId) {
            userService.getUser(id);
            allTasks.addAll(taskRepository.getTaskByUserId(id));
        }

        //поиск текущих задач
        for (Task task : allTasks)
               if (task.getStartDate().getTime() < new Date().getTime()
                       &&task.getEndDate().getTime() > new Date().getTime()){
                   currentTasks.add(task);
               }

        //сортировка текущих задач
        if (currentTasks.isEmpty()) {
            startingPosition = new Date().getTime();
        } else {
            Task startTask = currentTasks.get(0);
            for (Task task : currentTasks)
                if (startTask.getEndDate().getTime() > startTask.getEndDate().getTime())
                    startTask = task;
            startingPosition = startTask.getEndDate().getTime();
        }


        while (true) {
            int i;
            List<Task> tasksToProcess = new ArrayList<>();

            TimeIntervals timeInterval = new TimeIntervals();
            timeInterval.setStartDate(simpleDateFormat.format(new Date(startingPosition)));
            timeIntervals.add(timeInterval);

            for (Task task : allTasks)
                if (task.getStartDate().getTime() > startingPosition)
                    tasksToProcess.add(task);

            if (tasksToProcess.isEmpty())
                break;

            Task t = tasksToProcess.get(0);
            for (Task task : tasksToProcess)
                if (task.getStartDate().getTime() < t.getStartDate().getTime())
                    t = task;

            timeInterval.setEndDate(simpleDateFormat.format(t.getStartDate()));

            do {
                i = 0;
                for (Task task : tasksToProcess) {
                    if (t.getStartDate().getTime() <= task.getStartDate().getTime()
                            && t.getEndDate().getTime() >= task.getStartDate().getTime()
                            && t.getEndDate().getTime() < task.getEndDate().getTime()) {
                        t = task;
                        i++;
                    }
                }
            }while (i > 0);
            startingPosition = t.getEndDate().getTime();
        }
        timeIntervals.get(timeIntervals.size()-1).setEndDate("До скончания времён");
        return timeIntervals;
    }

}
