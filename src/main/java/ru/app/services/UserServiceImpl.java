package ru.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.app.models.User;
import ru.app.exceptions.ThisLoginAlreadyExistsException;
import ru.app.exceptions.UserNotFoundException;
import ru.app.repositories.UserRepository;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User addUser(User user) {
        if (userRepository.getUserByLogin(user.getLogin()).isPresent())
            throw new ThisLoginAlreadyExistsException("A user with this login is already in the database!");
        return userRepository.save(user);
    }

    @Override
    public User getUser(Long id) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
               return optionalUser.get();
        } else throw new UserNotFoundException("There is no user with this id :" + id);
    }
}

