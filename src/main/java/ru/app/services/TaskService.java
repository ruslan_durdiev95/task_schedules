package ru.app.services;

import ru.app.models.Task;
import ru.app.utils.TimeIntervals;

import java.util.List;
import java.util.Set;

public interface TaskService {
   Task createTaskUser(Set<Long> userId , Task task);
   List<TimeIntervals> getFreeTime(Set<Long> userId);
}
