package ru.app.services;

import ru.app.models.User;

public interface UserService {
   User addUser(User user);
   User getUser(Long id);
}
