package ru.app.mappers;

import org.springframework.stereotype.Component;
import ru.app.models.Task;
import ru.app.dto.TaskDto;
import ru.app.exceptions.IncorrectDateFormatException;
import ru.app.forms.TaskForm;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Component
public class TaskMapper {

    public Task parser(TaskForm taskForm){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
        Task task;
        try {
            task = Task.builder()
                    .title(taskForm.getTitle())
                    .startDate(simpleDateFormat.parse(taskForm.getStartDate()))
                    .endDate(simpleDateFormat.parse(taskForm.getEndDate()))
                    .build();
        } catch (ParseException e) {
            throw new IncorrectDateFormatException(e.getMessage());
        }
        return task;
    }

    public TaskDto parser(Task task){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
        return   TaskDto.builder()
                .title(task.getTitle())
                .startDate(simpleDateFormat.format(task.getStartDate()))
                .endDate(simpleDateFormat.format(task.getEndDate()))
                .build();
    }
}
