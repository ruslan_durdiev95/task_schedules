package ru.app.mappers;

import org.springframework.stereotype.Component;
import ru.app.models.User;
import ru.app.dto.UserDto;
import ru.app.forms.UserForm;

@Component
public class UserMapper {

    public User parser(UserForm userForm){
        return User.builder()
                .name(userForm.getName())
                .login(userForm.getLogin())
                .build();
    }
    public UserDto parser(User user){
        return UserDto.builder()
                .name(user.getName())
                .login(user.getLogin())
                .id(user.getId())
                .tasks(user.getTasks())
                .build();
    }

}
