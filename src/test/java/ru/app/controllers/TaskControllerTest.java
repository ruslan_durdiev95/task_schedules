package ru.app.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.app.models.Task;
import ru.app.models.User;
import ru.app.forms.TaskForm;
import ru.app.repositories.TaskRepository;
import ru.app.repositories.UserRepository;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class TaskControllerTest {
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ObjectMapper objectMapper;

    private User user1;
    private User user2;
    private User user3;
    private TaskForm taskForm;
    private Task task1;
    private Task task2;
    private Task task3;

    @BeforeEach
    void init(){
        user1 = User.builder()
                .name("admin1")
                .login("admin1")
                .build();
        user2 = User.builder()
                .name("admin2")
                .login("admin2")
                .build();
        user3 = User.builder()
                .name("admin3")
                .login("admin3")
                .build();
        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);

        task1 = Task.builder()
                .user(user1)
                .startDate(new Date(new Date().getTime()+500000000))
                .endDate(new Date(new Date().getTime()+700000000))
                .title("Задача")
                .build();

        task2 = Task.builder()
                .user(user2)
                .startDate(new Date(new Date().getTime()+800000000))
                .endDate(new Date(new Date().getTime()+900000000))
                .title("Задача")
                .build();

        task3= Task.builder()
                .user(user3)
                .startDate(new Date(new Date().getTime()+807000000))
                .endDate(new Date(new Date().getTime()+990000000))
                .title("Задача")
                .build();
    }


    @Test
    void addTaskUser() throws Exception {
        Set<Long> usersId = new HashSet<>();
        usersId.add(user1.getId());
        usersId.add(user2.getId());

        taskForm = TaskForm.builder()
                   .title("Задача")
                   .startDate(simpleDateFormat.format(new Date(new Date().getTime()+100000000)))
                   .endDate(simpleDateFormat.format(new Date(new Date().getTime()+200000000)))
                   .usersId(usersId)
                   .build();

        mockMvc.perform(post("/tasks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(taskForm)))
                .andExpect(status().isOk());
    }

    @Test
    void addTaskUserTheTaskAlreadyExistsInThisTimeInterval() throws Exception{
        Set<Long> usersId = new HashSet<>();
        usersId.add(user1.getId());
        taskRepository.save(task1);

        taskForm = TaskForm.builder()
                   .title("Задача")
                   .startDate(simpleDateFormat.format(task1.getStartDate()))
                   .endDate(simpleDateFormat.format(task1.getEndDate()))
                   .usersId(usersId)
                   .build();

        mockMvc.perform(post("/tasks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(taskForm)))
                .andExpect(status().is(403));
    }

    @Test
    void addTaskUserTheTaskCannotFinishBeforeItStarts() throws Exception{
        taskRepository.save(task1);

        taskForm = TaskForm.builder()
                .title("Задача")
                .startDate(simpleDateFormat.format(task1.getEndDate()))
                .endDate(simpleDateFormat.format(task1.getStartDate()))
                .usersId(Collections.singleton(user1.getId()))
                .build();

        mockMvc.perform(post("/tasks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(taskForm)))
                .andExpect(status().is(403));
    }


    @Test
    void getUsersFreeTimeOneUserOneTask() throws Exception{
        taskRepository.save(task1);
        mockMvc.perform(get("/tasks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(Collections.singleton(user1.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].startDate",
                        containsInAnyOrder(simpleDateFormat.format(new Date()) , simpleDateFormat.format(task1.getEndDate()))))
                .andExpect(jsonPath("$[*].endDate",
                        containsInAnyOrder(simpleDateFormat.format(task1.getStartDate()) , "До скончания времён")));
    }

    @Test
    void getUsersFreeTimeTwoUserTwoTask() throws Exception{
        Set<Long> usersId = new HashSet<>();
        usersId.add(user1.getId());
        usersId.add(user2.getId());
        taskRepository.save(task1);
        taskRepository.save(task2);

        mockMvc.perform(get("/tasks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(usersId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].startDate",
                        containsInAnyOrder(simpleDateFormat.format(new Date()) , simpleDateFormat.format(task1.getEndDate()) , simpleDateFormat.format(task2.getEndDate()))))
                .andExpect(jsonPath("$[*].endDate",
                        containsInAnyOrder(simpleDateFormat.format(task1.getStartDate()) , simpleDateFormat.format(task2.getStartDate()) , "До скончания времён")));
    }

    @Test
    void getUsersFreeTimeThreeUserThreeTask() throws Exception{
        Set<Long> usersId = new HashSet<>();
        usersId.add(user1.getId());
        usersId.add(user2.getId());
        usersId.add(user3.getId());
        taskRepository.save(task1);
        taskRepository.save(task2);
        taskRepository.save(task3);

        mockMvc.perform(get("/tasks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(usersId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].startDate",
                        containsInAnyOrder(simpleDateFormat.format(new Date()) ,  simpleDateFormat.format(task1.getEndDate()) , simpleDateFormat.format(task3.getEndDate()))))
                .andExpect(jsonPath("$[*].endDate",
                        containsInAnyOrder(simpleDateFormat.format(task1.getStartDate()) , simpleDateFormat.format(task2.getStartDate()) , "До скончания времён")));
    }

    @AfterEach
    void destroy(){
      taskRepository.deleteByTitle("Задача");
      userRepository.deleteByLogin(user1.getLogin());
      userRepository.deleteByLogin(user2.getLogin());
      userRepository.deleteByLogin(user3.getLogin());
    }
}